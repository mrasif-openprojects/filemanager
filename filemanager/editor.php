<?php
	session_start();
	$login=false;
	// Linked with www.mrasif.in
	if(isset($_SESSION['display'])){
		if($_SESSION['display']=="enabled_true"){
			$login=true;
		}
		else{
			$login=false;
		}
	}
	if($login==false){
		header("Location: ./login.php");
		exit();
	}
	$filepath="";
	if(isset($_GET['file'])){
		$filepath=$_GET['file'];
	}
	$patharray=split("/", $filepath);
	$filepath="";
	for ($i=0; $i<count($patharray) ; $i++) { 
		if($patharray[$i]!="" && $patharray[$i]!=".."){
			$filepath=$filepath."/".$patharray[$i];
		}
	}

?>
<?php
	include("./includes/config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="./icon/ico_folder.png" />
<link rel="stylesheet" type="text/css" href="./style/main.css" />
<title>File Manager</title>
</head>
<body style="background-color:#000000;" >
<table class="main" border="0"  cellpadding="0" cellspacing="0" align="center" style="background-color:#FFFFFF; padding:0px; width:900px;">
<tr>
<th colspan="4" id="title" style="font-size:36px; text-align:left; padding:5px;">File Manager</th>
<th colspan="2" style="font-size:25px; text-align:right; padding:5px;"><span style="cursor:pointer;" onclick="document.location.href='./logout.php';">Logout</span></th>
</tr>
<tr>
	<td align="left" style="padding-top:5px; padding-bottom:5px; width:130px; ">
		&nbsp;&nbsp;File Name:
	</td>
	<td align="left" colspan="4" style="padding-top:5px; padding-bottom:5px;">
		<input type="text" style="width:650px;" value="<?php 
			print($filepath);
		?>" readonly="true" />
	</td>
	<td align="right" style="padding-top:5px; padding-bottom:5px; width:100px; ">
		<input type="submit" value="Save" style="width:90px;" />&nbsp;&nbsp;
		<?php
			$filepath="../..".$filepath;
		?>
	</td>
</tr>
<tr>
	<td colspan="6">
		<textarea style="width:892px; height:500px;" id="textbox">Loading...</textarea>
	</td>
</tr>
<tr>
<th colspan="6" onclick="window.open('http://www.mrasif.in')" style="cursor:pointer;" title="Mr. Asif Mohammad Mollah<?php print("\n"); ?>    ( Software Engineer )">Developed by Mr. Asif</th>
</tr>
</table>

<script type="text/javascript">
	var xmlhttp;
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
  	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xmlhttp.onreadystatechange=function()
  	{
  		if (xmlhttp.readyState==4 && xmlhttp.status==200)
    	{
    		xmlDoc=xmlhttp.responseXML;
    		document.getElementById("textbox").innerHTML=xmlDoc;
    	}
  	}
	xmlhttp.open("GET","./fm/read.php?path=<?php print($filepath); ?>",true);
	xmlhttp.send();
</script>

</body>
</html>
<?php
	exit();
?>