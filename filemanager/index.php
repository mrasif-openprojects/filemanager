<?php
	session_start();
	$login=false;
	// Linked with www.mrasif.in
	if(isset($_SESSION['display'])){
		if($_SESSION['display']=="enabled_true"){
			$login=true;
		}
		else{
			$login=false;
		}
	}
	if($login==false){
		header("Location: ./login.php");
		exit();
	}
?>
<?php
	include("./includes/config.php");
	include("./includes/extension.php");
	$root=$ROOT_DIR;
	if(isset($_GET['path'])){
		$path=$_GET['path'];
	}
	else{
		$path="";
	}
	if($path!=""){
		$dir=$root."/".$path."/";
	}
	else{
		$dir=$root."/";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="./icon/ico_folder.png" />
<link rel="stylesheet" type="text/css" href="./style/main.css" />
<title>File Manager</title>
<script language="javascript" type="text/javascript">
	function rename(action,type,name){
		var toname=prompt("Enter New Name(With Extention):",name);
		//alert("Hello");
		if(toname!=null){
			document.location="./?path=<?php echo $path; ?>&action="+action+"&type="+type+"&name="+name+"&toname="+toname;
		}
	}
	function unzip(action,type,name){
			document.location="./?path=<?php echo $path; ?>&action="+action+"&type="+type+"&name="+name;
	}
	function edit(name){
		window.open("./editor.php?file=<?php print($dir); ?>"+name);
	}
	function back(){
		var url="<?php
				$aurl=explode("/",$path);
				//$s=$parts[count($parts)-1];
				$l=count($aurl)-1;
				if($l<=1){
					echo "?path=";
				}
				else{
					$i=1;
					$url="?path=";
					while($i<$l){
						$url=$url."/".$aurl[$i];
						$i=$i+1;
					}
					echo $url;
				}
			   ?>";
		document.location="./"+url;
	}
	function del(path,file,name){
		var ans=confirm("Delete ?\n"+name+" Name : "+file);
		if(ans==true){
			document.location=path;
		}
		else{
			//alert("Faild!");
		}
	}

</script>
</head>
<body style="background-color:#000000;" >
<table class="main" border="0"  cellpadding="0" cellspacing="0" align="center" style="background-color:#FFFFFF; padding:0px; width:900px;">
<tr>
<th colspan="4" id="title" style="font-size:36px; text-align:left; padding:5px;">File Manager</th>
<th colspan="2" style="font-size:25px; text-align:right; padding:5px;"><span style="cursor:pointer;" onclick="document.location.href='./logout.php';">Logout</span></th>
</tr>
<tr>
	<td colspan="3" align="center" valign="top" style="border:1px solid #0099CC;">
    	<H1 style="color:#0099CC;">Upload Files:</H1>
    	<form name="fileupload" action="./?path=<?php echo $path; ?>" enctype="multipart/form-data" method="post">
        <input type="file" name="file" style="border:1px solid black; width:220px;" /><br/>
        <input type="submit" value="Upload Files" style="border:1px solid black; width:220px; background-color:#999999;" />
        </form>
        <br/>
    </td>
    <td colspan="3" align="center" valign="top" style="border:1px solid #0099CC;">
    	<H1 style="color:#0099CC;">Create Folder:</H1>
    	<form name="rename" action="./?path=<?php echo $path; ?>" method="post">
        <input type="text" name="folder" style="border:1px solid black; width:220px;" /><br/>
        <input type="submit" value="Create" style="border:1px solid black; width:220px; background-color:#999999;" />
        </form>
    </td>
</tr>
<tr>
	<td colspan="6" align="center">
    	<?php
			ini_set("upload_max_filesize", "255M");
			ini_set("post_max_size", "256M");
			if(isset($_FILES['file'])){
				//$fn=$_POST['file'];
				move_uploaded_file($_FILES["file"]["tmp_name"],$dir . $_FILES["file"]["name"]);
				echo "Upload: " . $_FILES["file"]["name"] . "<br />";
				echo "Type: " . $_FILES["file"]["type"] . "<br />";
				echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
				echo "Stored in: $path/" . $_FILES["file"]["name"];
			}
		?>
		<?php
			if(isset($_POST['folder'])){
				$fn=$_POST['folder'];
				mkdir($dir.$fn);
				echo "Folder creation successfully !";
			}
		?>
    <?php
			if(isset($_GET['action']) && isset($_GET['name']) && isset($_GET['type'])){
				$action=$_GET['action'];
				$name=$_GET['name'];
				$type=$_GET['type'];
				$toname=$_GET['toname'];

				//deletion panel
				if($action=="delete" && $type=="folder"){
					rmdir($dir.$name);
					echo "Folder deletion successfully !";
				}
				elseif($action=="delete" && $type=="file"){
					unlink($dir.$name);
					echo "File deletion successfully !";
				}
				//rename panel
				if($action=="rename" && $type=="folder"){
					rename($dir.$name,$dir.$toname);
					echo "Folder deletion successfully !";
				}
				elseif($action=="rename" && $type=="file"){
					rename($dir.$name,$dir.$toname);
					echo "File deletion successfully !";
				}
				// Unzip
				if($action=="unzip" && $type=="file"){
					exec("cd ".$dir." && unzip ".$name,$out,$status);
					echo "Unziped successfully !";
				}

			}
		?>
    </td>
</tr>
<tr>
	<th width="70" valign="top"><img src="./icon/ico_back.gif" height="20" width="18" alt="Back" title="Back" onclick="back()" style="cursor:pointer;" />&nbsp;<sup>Icon</sup></th>
    <th width="300" align="left" valign="top">Name</th>
    <th width="80" align="left" valign="top">Types</th>
    <th width="120" align="right" valign="top" style='padding-right:15px;'>Size</th>
    <th width="160" valign="top">Last Modification</th>
    <th width="220" valign="top">Operation</th>
</tr>
<?php
	$folder = opendir($dir);
	$folderList=null;
	$fileList=null;
	$folder_index=0;
	$file_index=0;
	while (($entry = readdir($folder)) != ""){
		if($entry=="." || $entry==".." || "?path=$path/$entry"=="?path=/filemanager" || "?path=$path/$entry"=="?path=/FileManager" ){

		}
		elseif(gen($entry)=="" || filetype($dir."/".$entry)=="dir"){
			$folderList[$folder_index]=$entry;
			$folder_index=$folder_index+1;
		}
		else{
			$fileList[$file_index]=$entry;
			$file_index=$file_index+1;
		}
	}
	$folder = closedir($folder);


	$c=0;
	for($i=0; $i<$folder_index; $i++){
		$entry=$folderList[$i];
		if($c%2!=0){
				$c=$c+1;
				echo "<tr style='background-color:#CCFFCC;'>";
		}
		else{
			$c=$c+1;
			echo "<tr style='background-color:#FFFFCC;'>";
		}
		echo "<td align='center'><img src='./icon/ico_folder.png' /></td>";
		echo "<td><a href='?path=$path/$entry'>$entry</a></td>";
		echo "<td>Folder</td><td align='right' style='padding-right:15px;'><!--".round((filesize($dir."/".$entry)/1024),3)." KB--></td>";
		echo "<td align='right'>".date ("d M, Y H:i ",filemtime($dir."/".$entry))."&nbsp;&nbsp;</td>";
		echo "<td align='right'>
				<a href=javascript:del('?path=$path&action=delete&name=$entry&type=folder','$entry','Folder') >Delete</a>&nbsp;
				<a href=javascript:rename('rename','folder','$entry') >Rename</a>&nbsp;&nbsp;</td>";
		echo "</tr>";
		//print($folderList[$i]."   Folder<br/>");
	}
	for($i=0; $i<$file_index; $i++){
		$entry=$fileList[$i];
		if($c%2!=0){
				$c=$c+1;
				echo "<tr style='background-color:#CCFFCC;'>";
		}
		else{
			$c=$c+1;
			echo "<tr style='background-color:#FFFFCC;'>";
		}
		echo "<td align='center'>";
					echo "<img src='".getIconByExtension(gen($entry))."' />";
					echo "</td><td><a href='$path/$entry' target='_blank'>$entry</a></td>";
					echo "<td>Files</td><td align='right' style='padding-right:15px;'>".round((filesize($dir."/".$entry)/1024),3)." KB</td>";
					echo "<td align='right'>".date ("d M, Y H:i ",filemtime($dir."/".$entry))."&nbsp;&nbsp;</td>";
					echo "<td align='right'>
							<a href=javascript:unzip('unzip','file','$entry') >UnZip</a>&nbsp;
							<a href=javascript:del('?path=$path&action=delete&name=$entry&type=file','$entry','File') >Delete</a>&nbsp;
							<a href=javascript:rename('rename','file','$entry') >Rename</a>&nbsp;&nbsp;</td>";
					echo "</tr>";
		//print($fileList[$i]."   Files<br/>");

	}
?>
</tr>
<tr>
<th colspan="6" onclick="window.open('http://www.mrasif.in')" style="cursor:pointer;" title="Mr. Asif Mohammad Mollah<?php print("\n"); ?>    ( Software Engineer )">Developed by Mr. Asif</th>
</tr>
</table>
<?php
if(isset($_GET['di'])){
	$di=$_GET["di"];
}else{	$di="";}
function gen($fileName)
{
	$parts=explode(".",$fileName);
	$s=$parts[count($parts)-1];
	if($fileName==$s){
	$s="";
	}
	else{
	$s=$s;
	}
   return $s;
}
?>
</body>
</html>
<?php
	exit();
?>
